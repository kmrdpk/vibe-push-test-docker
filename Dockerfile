FROM java:8
EXPOSE 8080
ADD ./TestDataManager-1.0-SNAPSHOT.jar TestDataManager-1.0-SNAPSHOT.jar
ADD ./vibe-tree.json vibe-tree.json
ADD ./id_rsa /root/.ssh/
ADD ./id_rsa.pub /root/.ssh/
RUN chmod 600 /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa.pub

RUN touch /root/.ssh/known_hosts && \
	ssh-keyscan -H github.com >> /root/.ssh/known_hosts && \
	ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts && \
	echo "\nStrictHostKeyChecking no" >> /etc/ssh/ssh_config
ADD ./init.sh /init.sh
RUN chmod +x /init.sh
ENTRYPOINT ["/init.sh"]